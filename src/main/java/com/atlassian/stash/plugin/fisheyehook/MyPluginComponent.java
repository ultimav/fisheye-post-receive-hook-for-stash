package com.atlassian.stash.plugin.fisheyehook;

public interface MyPluginComponent
{
    String getName();
}