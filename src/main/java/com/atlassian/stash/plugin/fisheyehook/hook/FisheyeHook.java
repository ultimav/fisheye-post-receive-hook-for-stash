package com.atlassian.stash.plugin.fisheyehook.hook;

import com.atlassian.stash.hook.repository.*;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.setting.*;
import org.apache.commons.lang.StringUtils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;

public class FisheyeHook implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator
{
    /**
     * Connects to a configured URL to notify of all changes.
     */
    @Override
    public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges)
    {
        String url = context.getSettings().getString("url");
        if (url.length() > 0 && url.endsWith("/")){
            url = url.substring(0, url.length()-1);
        }

        String repositoryName = context.getSettings().getString("repository");
        if (StringUtils.isBlank(repositoryName)){
            repositoryName = context.getRepository().getName();
        }

        // We simply need to ping this REST endpoint in FishEye to trigger the indexing
        String endpoint = "/rest-service-fecru/admin/repositories-v1/".concat(repositoryName).concat("/scan");
        String token = context.getSettings().getString("token");
        url = url.concat(endpoint);

        if (url != null)
        {
            try
            {
                URLConnection connection = new URL(url).openConnection();
                HttpURLConnection http = (HttpURLConnection)connection;
                http.setDoOutput(true);
                http.setRequestMethod("POST");
                http.addRequestProperty("X-Api-Key", token); // Set the X-Api-Key header for authentication with FishEye
                http.getInputStream().close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository)
    {
        if (settings.getString("url", "").isEmpty())
        {
            errors.addFieldError("url", "Url field is blank, please supply one");
        }

        if (settings.getString("token", "").isEmpty())
        {
            errors.addFieldError("token", "You must provide an authentication token");
        }
    }
}